"use strict";

export const SERVER_IP_ADDRESS = process.env.IP || "0.0.0.0";
export const SERVER_PORT = 8081;
export const CLIENT_PORT = parseInt(process.env.PORT as string) || 2024;

export const buildConfg = {
	isDevelopmentMode: (process.env.NODE_ENV || "development").trim().toLowerCase() === "development",
	dev: {
		ipaddress: SERVER_IP_ADDRESS,
		port: SERVER_PORT,
		publicPath: `http://${SERVER_IP_ADDRESS}:${SERVER_PORT}/dist/`
	},
	realport: CLIENT_PORT,
	disabledChunkhash: false,
	hbs: {
		master: "index",
		extname: ".hbs"
	},
	paths: {
		src: {
			base: "./src/",

			views: "./src/server/views/",
			templates: "./src/server/views/templates/",
			partials: "./src/server/views/partials/",
			layouts: "./src/server/views/layouts/"
		},
		output: {
			base: "./wwwroot/",
			dist: "./wwwroot/dist/"
		}
	},
	urls: {
		publicPath: "./dist/"
	}
};
