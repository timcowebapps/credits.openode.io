"use strict";

import * as fs from "fs";
import * as path from "path";
import webpack from "webpack";
import webpackMerge from "webpack-merge";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import HtmlPlugin from "html-webpack-plugin";
import { SERVER_PORT, CLIENT_PORT, buildConfg } from "./../buildconfig";
import { ModeEnum } from "./foundation/modeenum";
import { getClientBaseConfig } from "./clientconfig.base";

const ROOT_DIR = path.resolve(__dirname, "./../../");
const packageJson: any = JSON.parse(fs.readFileSync(path.resolve("./package.json")).toString());

export const clientDevConfig: webpack.Configuration = webpackMerge(getClientBaseConfig(ModeEnum.Development), {
	name: "ClientDevConfg",
	mode: ModeEnum.Development,
	devtool: "cheap-module-source-map",
	entry: {
		"client-bundle": [
			`webpack-dev-server/client?http://${buildConfg.dev.ipaddress}:${buildConfg.dev.port}`,
			"webpack/hot/only-dev-server",
			path.resolve(ROOT_DIR, buildConfg.paths.src.base, "client/entry")
		]
	},
	output: {
		filename: buildConfg.disabledChunkhash
			? "[name].js"
			: "[name].[hash:8].js",
		sourceMapFilename: buildConfg.disabledChunkhash
			? "[name].map"
			: "[name].[hash:8].map",
		chunkFilename: buildConfg.disabledChunkhash
			? "[name].chunk.js"
			: "[name].[hash:8].chunk.js",
		hotUpdateChunkFilename: "hot/hot-update.js",
		hotUpdateMainFilename: "hot/hot-update.json",
		publicPath: buildConfg.dev.publicPath
	},
	plugins: [
		new webpack.DefinePlugin({
			"process.env": {
				NODEENV: JSON.stringify(ModeEnum.Development),
				BROWSER: JSON.stringify(true),
				VERSION: JSON.stringify(packageJson.version)
			}
		}),
		new HtmlPlugin({
			title: packageJson.name + ' - ' + packageJson.description,
			template: path.resolve(ROOT_DIR, buildConfg.paths.src.layouts, buildConfg.hbs.master + buildConfg.hbs.extname),
			filename: path.resolve(ROOT_DIR, buildConfg.paths.output.base, buildConfg.hbs.master + buildConfg.hbs.extname),
			minify: false,
			hash: true,
			inject: false,
			urlContent: (content: string) => content
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin()
	]
});
