"use strict";

import * as fs from "fs";
import * as path from "path";
import webpack from "webpack";
import webpackMerge from "webpack-merge";
import TerserPlugin from "terser-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import HtmlPlugin from "html-webpack-plugin";
import { buildConfg } from "./../buildconfig";
import { ModeEnum } from "./foundation/modeenum";
import { getClientBaseConfig } from "./clientconfig.base";

const ROOT_DIR = path.resolve(__dirname, "./../../");
const packageJson: any = JSON.parse(fs.readFileSync(path.resolve("./package.json")).toString());

export const clientConfig: webpack.Configuration = webpackMerge(getClientBaseConfig(ModeEnum.Production), {
	name: "ClientConfig",
	mode: ModeEnum.Production,
	devtool: "inline-source-map",
	node: {
		fs: "empty"
	},
	entry: {
		"client-bundle": path.resolve(ROOT_DIR, buildConfg.paths.src.base, "client/entry.ts")
	},
	output: {
		filename: buildConfg.disabledChunkhash
			? "[name].js"
			: "[name].[chunkhash:8].js",
		sourceMapFilename: buildConfg.disabledChunkhash
			? "[name].map"
			: "[name].[chunkhash:8].map",
		chunkFilename: buildConfg.disabledChunkhash
			? "[name].chunk.js"
			: "[name].[chunkhash:8].chunk.js",
		publicPath: buildConfg.urls.publicPath
	},
	optimization: {
		minimizer: [
			new TerserPlugin({
				cache: true,
				parallel: true,
				sourceMap: true
			})
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: buildConfg.disabledChunkhash
				? "[name].css"
				: "[name].[contenthash:8].css",
			chunkFilename: buildConfg.disabledChunkhash
				? "[name].chunk.css"
				: "[name].[contenthash:8].chunk.css"
		}),
		new webpack.optimize.OccurrenceOrderPlugin(true),
		new webpack.DefinePlugin({
			"process.env": {
				NODEENV: JSON.stringify(ModeEnum.Production),
				BROWSER: JSON.stringify(true),
				VERSION: JSON.stringify(packageJson.version)
			}
		}),
		new HtmlPlugin({
			title: packageJson.name + ' - ' + packageJson.description,
			template: path.resolve(ROOT_DIR, buildConfg.paths.src.layouts, buildConfg.hbs.master + buildConfg.hbs.extname),
			filename: path.resolve(ROOT_DIR, buildConfg.paths.output.base, buildConfg.hbs.master + buildConfg.hbs.extname),
			hash: false,
			cache: true,
			showErrors: false,
			inject: false,
			urlContent: (content) => content,
			minify: {
				collapseWhitespace: true,
				collapseInlineTagWhitespace: true,
				removeComments: true,
				removeRedundantAttributes: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true
			}
		})
	]
});
