"use strict";

import path from "path";
import webpack from "webpack";
import NodeExternals from "webpack-node-externals";
import { buildConfg } from "./../buildconfig";
import { ModeEnum } from "./foundation/modeenum";
import { TargetEnum } from "./foundation/targetenum";
import { getRules } from "./common/rules";
import { getAlias } from "./common/alias";

const ROOT_DIR = path.resolve(__dirname, "./../../");

export const getServeBaseConfig = (mode: ModeEnum): webpack.Configuration => {
	return {
		target: TargetEnum.Node,
		entry: {
			"server-bundle": path.resolve(ROOT_DIR, buildConfg.paths.src.base, "server/entry.ts")
		},
		output: {
			path: path.resolve(ROOT_DIR, buildConfg.paths.output.dist),
			libraryTarget: "commonjs2"
		},
		externals: NodeExternals({
			modulesDir: path.resolve(ROOT_DIR, "./node_modules")
		}),
		module: {
			rules: [...getRules(mode, TargetEnum.Node)]
		},
		resolve: {
			extensions: [".js", ".ts", ".css", ".scss", ".json"],
			modules: [
				path.resolve(ROOT_DIR, "./node_modules")
			],
			alias: getAlias()
		},
		plugins: []
	}
};
