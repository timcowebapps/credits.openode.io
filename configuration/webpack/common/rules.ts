"use strict";

import webpack from "webpack";
import { ModeEnum } from "./../foundation/modeenum";
import { TargetEnum } from "./../foundation/targetenum";
import * as rules from "./rules/_exports";

export const getRules = (mode: ModeEnum, target: TargetEnum): Array<webpack.RuleSetRule> => ([
	rules.tsRule(),
	rules.styleRule({ mode, target }),
	rules.assetRule()
]);
