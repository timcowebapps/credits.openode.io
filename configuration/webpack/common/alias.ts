"use strict";

import path from "path";

export const getAlias = (): {
	[key: string]: string
} => {
	return {
		"timcowebapps/common.ooscss": path.resolve(__dirname, "./../../../node_modules/@timcowebapps/common.ooscss"),
		"timcowebapps/common.smascss.styles": path.resolve(__dirname, "./../../../node_modules/@timcowebapps/common.smascss.styles")
	}
};
