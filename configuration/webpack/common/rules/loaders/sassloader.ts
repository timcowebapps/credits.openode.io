"use strict";

import webpack from "webpack";

export const sassLoader = (): webpack.RuleSetLoader => ({
	loader: "sass-loader"
});
