"use strict";

import webpack from "webpack";
import autoprefixer from "autoprefixer";

export const postcssLoader = (): webpack.RuleSetLoader => ({
	loader: "postcss-loader",
	options: {
		plugins: () => [
			autoprefixer()
		],
		sourceMap: true
	}
});
