"use strict";

import { ModeEnum } from "../../foundation/modeenum";
import { TargetEnum } from "../../foundation/targetenum";

export interface IStyleRuleOptions {
	mode: ModeEnum;
	target: TargetEnum;
};
