"use strict";

export enum TargetEnum {
	Node = "node",
	Web = "web"
};
