"use strict";

export enum ModeEnum {
	Production = "production",
	Development = "development"
};
