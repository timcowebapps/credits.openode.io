"use strict";

import path from "path";
import webpack from "webpack";
import { buildConfg } from "./../buildconfig";
import { ModeEnum } from "./foundation/modeenum";
import { TargetEnum } from "./foundation/targetenum";
import { getRules } from "./common/rules";
import { getAlias } from "./common/alias";

const ROOT_DIR = path.resolve(__dirname, "./../../");

export const getClientBaseConfig = (mode: ModeEnum): webpack.Configuration => {
	return {
		target: TargetEnum.Web,
		output: {
			path: path.resolve(ROOT_DIR, buildConfg.paths.output.dist)
		},
		performance: {
			maxEntrypointSize: 512000,
			maxAssetSize: 512000
		},
		module: {
			rules: [...getRules(mode, TargetEnum.Web)]
		},
		resolve: {
			extensions: [".js", ".ts", ".hbs"],
			modules: [
				path.resolve(ROOT_DIR, "./node_modules")
			],
			alias: getAlias()
		}
	}
};
