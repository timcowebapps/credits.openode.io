FROM node:12-alpine

ENV NODE_ENV production
ENV NPM_CONFIG_PRODUCTION false
ENV PORT=2024

WORKDIR /opt/app
COPY . ./

RUN echo 'crond' > /boot.sh
# RUN echo 'crontab .openode.cron' >> /boot.sh

RUN npm i
RUN npm run build

CMD sh /boot.sh && npm run start-server
