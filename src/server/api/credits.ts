"use strict";

import * as express from "express";

const router = express.Router();
router.get("/", async (request: express.Request, response: express.Response, next: express.NextFunction) => {
	try {
		response.json({
			message: "test"
		});
	} catch (e) {
		response.sendStatus(500);
	}
});

module.exports = router;
