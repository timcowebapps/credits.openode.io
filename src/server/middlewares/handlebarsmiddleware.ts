"use strict";

import fs from "fs";
import path from "path";
import * as express from "express";
import { buildConfg } from "../../../configuration/buildconfig";

export namespace HandlebarsMiddleware {
	export function handler(request: express.Request, response: express.Response, options: any) {
		var templateExtnameRegex = new RegExp(".hbs" + '$');
		var templateScripts = fs.readdirSync(path.resolve(process.cwd(), buildConfg.paths.src.templates)).map((filename: string) => {
			var template = fs.readFileSync(path.resolve(process.cwd(), buildConfg.paths.src.templates, filename), {
				encoding: "utf8"
			});

			return "<script type='text/x-handlebars-template' id='" + filename.replace(templateExtnameRegex, "") + "-template'>"
				+ template + "</script>";
		}).join("");

		response.render(options.viewname, {
			...options.payload,
			templateScripts
		});
	};
}
