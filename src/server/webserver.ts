"use strict";

import path from "path";
import * as _ from "underscore";
import * as express from "express";
import * as exphbs from "express-handlebars";
import * as Handlebars from "handlebars";
import * as HandlebarsIntl from "handlebars-intl";
import * as http from "http";
import bodyParser from "body-parser";
import StrictEventEmitter from "strict-event-emitter-types";
import { EventEmitter } from "events";
import { Methodology } from "@timcowebapps/common.ooscss";
import { buildConfg } from "../../configuration/buildconfig";
import { HandlebarsMiddleware } from "./middlewares/handlebarsmiddleware";

const styles = require("./../shared/themes/main.scss");

interface WebServerEventEmitter {
	listening: (this: WebServer) => this;
}

export class WebServer extends (EventEmitter as new () => StrictEventEmitter<EventEmitter, WebServerEventEmitter>) {
	private app_: express.Express = express.default();
	public readonly httpserver: http.Server = http.createServer(this.app_);

	/**
	 * Конструктор класса.
	 * 
	 * @class WebServer
	 * @public
	 * @constructor
	 */
	public constructor(
		private readonly ipaddress: string,
		private readonly port: number) {
		super();

		HandlebarsIntl.__addLocaleData({ locale: "ru" });
		HandlebarsIntl.registerWith(Handlebars);

		Handlebars.registerHelper("bem", (context: {
			hash: { block: string; elem: string; mods: string; }
		}, options: Handlebars.HelperOptions) => {

			return Methodology.Bem.Entities.block(styles.locals, context.hash.block)
				.element(context.hash.elem).modifiers(eval(context.hash.mods));
		});

		this.configure();
		this.routes();
	}

	public async listen(): Promise<void> {
		await new Promise<void>(resolve => {
			this.httpserver.listen(this.port, this.ipaddress, () => {
				resolve();
			});
		});

		this.emit("listening");
	}

	public async close(): Promise<void> {
		await new Promise<void>(resolve => {
			if (this.httpserver.listening) {
				this.httpserver.close(() => {
					resolve();
				});
			}
		});
	}

	/**
	 * Конфигурация приложения.
	 * 
	 * @class WebServer
	 * @public
	 * @method configure
	 */
	public configure(): void {
		this.app_.use(bodyParser.urlencoded({ extended: true }));
		this.app_.use(bodyParser.json());

		/** Добавляем статические пути. */
		this.app_.use(express.static(path.resolve(process.cwd(), buildConfg.paths.output.base)));
		this.app_.use(express.static(path.resolve(process.cwd(), buildConfg.paths.output.dist)));

		this.app_.set("view engine", "hbs");
		this.app_.set("views", path.resolve(process.cwd(), buildConfg.paths.src.views));
		this.app_.engine("hbs", exphbs.create({
			defaultLayout: buildConfg.hbs.master,
			extname: buildConfg.hbs.extname,
			layoutsDir: path.resolve(process.cwd(), buildConfg.paths.output.base),
			partialsDir: [
				path.resolve(process.cwd(), buildConfg.paths.src.views),
				path.resolve(process.cwd(), buildConfg.paths.src.partials)
			]
		}).engine);
	}

	public routes() {
		// API Routes
		this.app_.use("/api/credits", require("./api/credits"));

		// SSR Routes
		const router = express.Router();
		router.get("/", this.homeHandler.bind(this));
		this.app_.use(router);

		// 404
		this.app_.use("*", (request: express.Request, response: express.Response, next: express.NextFunction) => {
			response.status(404).render("404", {
				title: "404"
			});
		});
	}

	public homeHandler(request: express.Request, response: express.Response, next: express.NextFunction) {
		HandlebarsMiddleware.handler(request, response, {
			viewname: "home",
			payload: {
				// Empty
			}
		});
	}
};
